package tu160.currencyconverter;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.ref.PhantomReference;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

public class MainActivity extends AppCompatActivity {
    private EditText editText01;
    private Button bnt01;
    private TextView textView01;
    private String usd;

    private static final String url = "https://api.fixer.io/latest?base=USD";
    String json = "";
    String line = "";
    String rate = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editText01 = (EditText) findViewById(R.id.EditText01);
        textView01 = (TextView) findViewById(R.id.Yen);

        bnt01 = (Button) findViewById(R.id.bnt);
        bnt01.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View convertToYen){
                System.out.println("\nTest1 before Asynch execution\n");
                BackgroundTask convertYen = new BackgroundTask();
                convertYen.execute();
                System.out.println("\nTest2 after Asynch execution\n");
            }
        });
    }
    private class BackgroundTask extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                URL web_url = new URL(MainActivity.this.url);
                HttpURLConnection httpURLConnection = (HttpURLConnection) web_url.openConnection();
                httpURLConnection.setRequestMethod("GET");
                System.out.println("\nTesting\n");
                httpURLConnection.connect();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                System.out.println("Connection Succ");
                while (line != null) {
                    json += line;
                }
                System.out.println("\nThe JSON: " + json);
                JSONObject obj = new JSONObject();
                JSONObject objRate = obj.getJSONObject("rates");
                rate = objRate.get("JPY").toString();
                System.out.println("\nWhat is rate: " + rate + "\n");

                //Convert rate into a double
                double exchangeRate = Double.parseDouble(rate);
                System.out.println("\nTesting KSON String Exchange Rate INSIDE AsynchTask:");

                usd = editText01.getText().toString();
                if(usd.equals("")){
                    textView01.setText("This field cannot be blank");
                }else{
                    Double dInputs = Double.parseDouble(usd);
                    Double result = dInputs * exchangeRate;
                    textView01.setText("$"+usd+" = " + "Y"+String.format("%.2f", result));
                    editText01.setText("");
                }

            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e){
                Log.e("MYAPP","Bad things happened", e);
                System.exit(1);
            }
                return null;


        }
    }
    }




